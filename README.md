# install-oracle-18c-centos7_4

1. preparation after installation of CentOS 7.4

```bash
#### running under root
dd if=/dev/zero of=/mnt/1GB.swap bs=1G count=1
mkswap /mnt/1GB.swap
swapon /mnt/1GB.swap
echo "/mnt/1GB.swap none swap sw 0 0" >>/etc/fstab
```
2. run install

download oracle file LINUX.X64_180000_db_home.zip from oradle site and copy it to the install folder
<http://www.oracle.com/technetwork/database/enterprise-edition/downloads/index.html>

```bash 
#### running under root
chmod 755 ./*.sh
./installoracle.sh
cp -f oracleService.sh /opt/oracle/
cp -f oracle.service /usr/lib/systemd/system
```
3. change environment in /etc/profile.d/sh.local
```bash
export ORACLE_BASE=/opt/oracle
export ORACLE_HOME=/opt/oracle/product/18c/dbhome_1
export PATH=$ORACLE_HOME/bin:$ORACLE_HOME/OPatch/:/usr/sbin:$PATH
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
export CLASSPATH=$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib
export ORACLE_SID=ORCLCDB
export ORACLE_PDB=ORCLPDB
```
4. start/stop or enable/disable service
```bash
#### running service command under root
#start
systemctl start oracle
#stop
systemctl enable oracle
#enable
systemctl stop oracle
#disable
systemctl disable oracle
#show status
systemctl status oracle
```

