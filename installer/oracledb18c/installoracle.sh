#!/bin/bash
export ORACLE_BASE=/opt/oracle
export ORACLE_HOME=/opt/oracle/product/18c/dbhome_1
export INSTALL_DIR=/opt/install
export INSTALL_FILE_1="LINUX.X64_180000_db_home.zip"
export INSTALL_RSP="db_inst.rsp"
export CONFIG_RSP="dbca.rsp.tmpl"
export PWD_FILE="setPassword.sh"
export RUN_FILE="runOracle.sh"
export START_FILE="startDB.sh"
export CREATE_DB_FILE="createDB.sh"
export SETUP_LINUX_FILE="setupLinuxEnv.sh"
export CHECK_SPACE_FILE="checkSpace.sh"
export CHECK_DB_FILE="checkDBStatus.sh"
export USER_SCRIPTS_FILE="runUserScripts.sh"
export INSTALL_DB_BINARIES_FILE="installDBBinaries.sh"
export PATH=$ORACLE_HOME/bin:$ORACLE_HOME/OPatch/:/usr/sbin:$PATH
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
export CLASSPATH=$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib
export ORACLE_SID=ORCLCDB
export ORACLE_PDB=ORCLPDB
export ORACLE_PWD=QWEzxc012345
export ORACLE_CHARACTERSET=AL32UTF8
mkdir $INSTALL_DIR
mkdir $ORACLE_BASE
cp $SETUP_LINUX_FILE $INSTALL_DIR/
cp $CHECK_SPACE_FILE $INSTALL_DIR/
cp $RUN_FILE          $ORACLE_BASE/
cp $START_FILE        $ORACLE_BASE/
cp $CREATE_DB_FILE    $ORACLE_BASE/
cp $CONFIG_RSP        $ORACLE_BASE/
cp $PWD_FILE          $ORACLE_BASE/
cp $CHECK_DB_FILE     $ORACLE_BASE/
cp $USER_SCRIPTS_FILE $ORACLE_BASE/
cp oracleService.sh   $ORACLE_BASE/

chmod ug+x $INSTALL_DIR/*.sh && \
    sync && \
    $INSTALL_DIR/$CHECK_SPACE_FILE && \
    $INSTALL_DIR/$SETUP_LINUX_FILE && \
    rm -rf $INSTALL_DIR/*

cp $INSTALL_FILE_1           $INSTALL_DIR/
cp $INSTALL_RSP              $INSTALL_DIR/
cp $INSTALL_DB_BINARIES_FILE $INSTALL_DIR/	
chown oracle:dba  $INSTALL_DIR
chown oracle:dba  $ORACLE_BASE
chown oracle:dba  $ORACLE_HOME
chown oracle:dba  $INSTALL_DIR/$INSTALL_FILE_1
chown oracle:dba  $INSTALL_DIR/$INSTALL_RSP
chown oracle:dba  $INSTALL_DIR/$INSTALL_DB_BINARIES_FILE
su -m --command "$INSTALL_DIR/$INSTALL_DB_BINARIES_FILE EE" oracle
$ORACLE_BASE/oraInventory/orainstRoot.sh 
$ORACLE_HOME/root.sh
