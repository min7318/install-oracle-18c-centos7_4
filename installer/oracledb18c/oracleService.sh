#!/bin/bash

case "$1" in 
start)
   cd /opt/oracle
   source /etc/profile
   /opt/oracle/runOracle.sh &
   echo $!>/var/run/runOracle.pid
   ;;
stop)
   kill `cat /var/run/runOracle.pid`
   rm /var/run/runOracle.pid
   ;;
restart)
   $0 stop
   $0 start
   ;;
status)
   if [ -e /var/run/runOracle.pid ]; then
      echo runOracle.sh is running, pid=`cat /var/run/runOracle.pid`
   else
      echo runOracle.sh is NOT running
      exit 1
   fi
   ;;
*)
   echo "Usage: $0 {start|stop|status|restart}"
esac

exit 0 