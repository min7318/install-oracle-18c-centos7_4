#!/bin/bash
# LICENSE UPL 1.0
#
# Copyright (c) 1982-2018 Oracle and/or its affiliates. All rights reserved.
# 
# Since: November, 2016
# Author: gerald.venzl@oracle.com
# Description: Runs the Oracle Database inside the container
# 
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
# 
export ORACLE_BASE=/opt/oracle
export ORACLE_HOME=/opt/oracle/product/18c/dbhome_1
export INSTALL_DIR=/opt/install
export INSTALL_FILE_1="LINUX.X64_180000_db_home.zip"
export INSTALL_RSP="db_inst.rsp"
export CONFIG_RSP="dbca.rsp.tmpl"
export PWD_FILE="setPassword.sh"
export RUN_FILE="runOracle.sh"
export START_FILE="startDB.sh"
export CREATE_DB_FILE="createDB.sh"
export SETUP_LINUX_FILE="setupLinuxEnv.sh"
export CHECK_SPACE_FILE="checkSpace.sh"
export CHECK_DB_FILE="checkDBStatus.sh"
export USER_SCRIPTS_FILE="runUserScripts.sh"
export INSTALL_DB_BINARIES_FILE="installDBBinaries.sh"
export PATH=$ORACLE_HOME/bin:$ORACLE_HOME/OPatch/:/usr/sbin:$PATH
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
export CLASSPATH=$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib
export ORACLE_SID=ORCLCDB
export ORACLE_PDB=ORCLPDB
export ORACLE_PWD=QWEzxc012345
export ORACLE_CHARACTERSET=AL32UTF8

########### Move DB files ############
function moveFiles {

   if [ ! -d $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID ]; then
      mkdir -p $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/
   fi;

   mv $ORACLE_HOME/dbs/spfile$ORACLE_SID.ora $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/
   mv $ORACLE_HOME/dbs/orapw$ORACLE_SID $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/
   mv $ORACLE_HOME/network/admin/sqlnet.ora $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/
   mv $ORACLE_HOME/network/admin/listener.ora $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/
   mv $ORACLE_HOME/network/admin/tnsnames.ora $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/

   # oracle user does not have permissions in /etc, hence cp and not mv
   cp /etc/oratab $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/
   chown -R oracle:dba $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID
   symLinkFiles;
}

########### Symbolic link DB files ############
function symLinkFiles {

   if [ ! -L $ORACLE_HOME/dbs/spfile$ORACLE_SID.ora ]; then
      ln -s $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/spfile$ORACLE_SID.ora $ORACLE_HOME/dbs/spfile$ORACLE_SID.ora
   fi;
   
   if [ ! -L $ORACLE_HOME/dbs/orapw$ORACLE_SID ]; then
      ln -s $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/orapw$ORACLE_SID $ORACLE_HOME/dbs/orapw$ORACLE_SID
   fi;
   
   if [ ! -L $ORACLE_HOME/network/admin/sqlnet.ora ]; then
      ln -s $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/sqlnet.ora $ORACLE_HOME/network/admin/sqlnet.ora
   fi;

   if [ ! -L $ORACLE_HOME/network/admin/listener.ora ]; then
      ln -s $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/listener.ora $ORACLE_HOME/network/admin/listener.ora
   fi;

   if [ ! -L $ORACLE_HOME/network/admin/tnsnames.ora ]; then
      ln -s $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/tnsnames.ora $ORACLE_HOME/network/admin/tnsnames.ora
   fi;

   # oracle user does not have permissions in /etc, hence cp and not ln 
   cp $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID/oratab /etc/oratab

}

########### SIGINT handler ############
function _int() {
   echo "Stopping container."
   echo "SIGINT received, shutting down database!"
   su -m --command "sqlplus / as sysdba << EOF
   shutdown immediate;
   exit;
EOF" oracle
   su -m --command "lsnrctl stop" oracle
   kill $childPID
}

########### SIGTERM handler ############
function _term() {
   echo "Stopping container."
   echo "SIGTERM received, shutting down database!"
   su -m --command "sqlplus / as sysdba << EOF
   shutdown immediate;
   exit;
EOF" oracle
   su -m --command "lsnrctl stop" oracle
   kill $childPID
}

########### SIGKILL handler ############
function _kill() {
   echo "SIGKILL received, shutting down database!"
   su -m --command "sqlplus / as sysdba << EOF
   shutdown abort;
   exit;
EOF" oracle
   su -m --command "lsnrctl stop" oracle
   kill $childPID
}

###################################
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! #
############# MAIN ################
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! #
###################################

# Check whether container has enough memory
# Github issue #219: Prevent integer overflow,
# only check if memory digits are less than 11 (single GB range and below) 
if [ `cat /sys/fs/cgroup/memory/memory.limit_in_bytes | wc -c` -lt 11 ]; then
   if [ `cat /sys/fs/cgroup/memory/memory.limit_in_bytes` -lt 2147483648 ]; then
      echo "Error: The container doesn't have enough memory allocated."
      echo "A database container needs at least 2 GB of memory."
      echo "You currently only have $((`cat /sys/fs/cgroup/memory/memory.limit_in_bytes`/1024/1024/1024)) GB allocated to the container."
      exit 1;
   fi;
fi;

# Set SIGINT handler
trap _int SIGINT

# Set SIGTERM handler
trap _term SIGTERM

# Set SIGKILL handler
trap _kill SIGKILL


# Default for ORACLE SID
if [ "$ORACLE_SID" == "" ]; then
   export ORACLE_SID=ORCLCDB
else
  # Make ORACLE_SID upper case
  # Github issue # 984
  export ORACLE_SID=${ORACLE_SID^^}

  # Check whether SID is no longer than 12 bytes
  # Github issue #246: Cannot start OracleDB image
  if [ "${#ORACLE_SID}" -gt 12 ]; then
     echo "Error: The ORACLE_SID must only be up to 12 characters long."
     exit 1;
  fi;

  # Check whether SID is alphanumeric
  # Github issue #246: Cannot start OracleDB image
  if [[ "$ORACLE_SID" =~ [^a-zA-Z0-9] ]]; then
     echo "Error: The ORACLE_SID must be alphanumeric."
     exit 1;
   fi;
fi;

# Default for ORACLE PDB
export ORACLE_PDB=${ORACLE_PDB:-ORCLPDB1}

# Make ORACLE_PDB upper casejavascript:void(0)
# Github issue # 984
export ORACLE_PDB=${ORACLE_PDB^^}

# Default for ORACLE CHARACTERSET
export ORACLE_CHARACTERSET=${ORACLE_CHARACTERSET:-AL32UTF8}

# Check whether database already exists
if [ -d $ORACLE_BASE/oradata/$ORACLE_SID ]; then
   symLinkFiles;
   
   # Make sure audit file destination exists
   if [ ! -d $ORACLE_BASE/admin/$ORACLE_SID/adump ]; then
      mkdir -p $ORACLE_BASE/admin/$ORACLE_SID/adump
   fi;
   
   # Start database
   echo "starting database.";
   su -m --command "$ORACLE_BASE/$START_FILE" oracle ;
   
else
  # Remove database config files, if they exist
  rm -f $ORACLE_HOME/dbs/spfile$ORACLE_SID.ora ;
  rm -f $ORACLE_HOME/dbs/orapw$ORACLE_SID ;
  rm -f $ORACLE_HOME/network/admin/sqlnet.ora ;
  rm -f $ORACLE_HOME/network/admin/listener.ora ;
  rm -f $ORACLE_HOME/network/admin/tnsnames.ora ;
  rm -rf $ORACLE_BASE/oradata/$ORACLE_SID ;
  rm -rf $ORACLE_BASE/admin/$ORACLE_SID ;
  rm -rf $ORACLE_BASE/oradata/dbconfig/$ORACLE_SID ;
  # Create database
  echo "creating database.";
  $ORACLE_BASE/$CREATE_DB_FILE $ORACLE_SID $ORACLE_PDB $ORACLE_PWD;
   
  # Move database operational files to oradata
  chown oracle:dba $ORACLE_HOME/dbs/spfile$ORACLE_SID.ora ;
  chown oracle:dba $ORACLE_HOME/dbs/orapw$ORACLE_SID ;
  chown oracle:dba $ORACLE_HOME/network/admin/sqlnet.ora ;
  chown oracle:dba $ORACLE_HOME/network/admin/listener.ora ;
  chown oracle:dba $ORACLE_HOME/network/admin/tnsnames.ora ;
  moveFiles;
   
  # Execute custom provided setup scripts
  echo "customer scripting.";
  su -m --command "$ORACLE_BASE/$USER_SCRIPTS_FILE $ORACLE_BASE/scripts/setup" oracle ;
fi;

# Check whether database is up and running
su -m --command "$ORACLE_BASE/$CHECK_DB_FILE" oracle
if [ $? -eq 0 ]; then
  echo "#########################" ;
  echo "DATABASE IS READY TO USE!" ;
  echo "#########################" ;
  
  # Execute custom provided startup scripts
su -m --command "$ORACLE_BASE/$USER_SCRIPTS_FILE $ORACLE_BASE/scripts/startup" oracle ;
  
else
  echo "#####################################" ;
  echo "########### E R R O R ###############" ;
  echo "DATABASE SETUP WAS NOT SUCCESSFUL!" ;
  echo "Please check output for further info!" ;
  echo "########### E R R O R ###############" ; 
  echo "#####################################" ;
fi;

# Tail on alert log and wait (otherwise container will exit)
echo "The following output is now a tail of the alert.log:"
tail -f $ORACLE_BASE/diag/rdbms/*/*/trace/alert*.log &
childPID=$!
wait $childPID
